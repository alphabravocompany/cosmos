# Welcome to Cosmos
Cosmos is an opinionated application stack which can be used to deploy and manage your Kubernetes stack with CI/CD via ArgoCD.

## Values Format
The values.yaml file is used to pass application configuration data, and can be used to deploy manifests via GIT or Helm charts.<br />
<br />
The values.yaml file is layed out as follows:
```
applications: 
    <application>:
        name: ## Name of the application. No dashes, underscores, or spaces
        destination: 
            server: ## Optional. Default: https://kubernetes.default.svc
            namespace: ## Application namespace. Default: default
        source:
            type: ## Options: manifest, helm
            repoURL: ## Options: GIT, Helm Chart
            targetRevision: ## Examples: Sem Version Number, GIT Branch

            ## Manifest Option
            path: ## Path to manifest in GIT

            ## Helm Option
            chart: ## Helm Chart
            values: ## Values to pass to the Helm chart.
```

### Helm Example
```
certmanager:
    name: cert-manager
    destination: 
        namespace: cert-manager
    source:
        type: helm      
        repoURL: https://charts.jetstack.io
        chart: cert-manager
        targetRevision: "v1.4.0"
        values: |
        installCRDs: true
```

### Manifest Example
```
logdna:
    name: logdna
    destination: 
        namespace: logdna-agent
    source:
        type: manifest
        repoURL: https://gitlab.com/alphabravocompany/cosmos.git
        path: services/logdna
        targetRevision: main
```